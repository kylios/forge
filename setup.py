from setuptools import setup

DESCRIPTION = "A simple static site generator"

LONG_DESCRIPTION = '''
Forge is a static site generator that converts markdown into static html webpages. Multiple content types are supported, with different layouts for each.
'''  # NOQA

config = dict(
    name="forge",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    author="Kyle Racette",
    author_email="kracette@gmail.com",
    license="GPLv3",
    packages=[
        'forge',
        'forge.exc'
    ],
    install_requires=[
        'Jinja2',
        'Markdown',
        'toml',
        'watchdog',
    ],
    entry_points={
        'console_scripts': [
            'forge-generate=forge.__main__:main',
            'forge-create-post=forge.__main__:create_post',
            'forge-serve=forge.__main__:serve',
            'forge-watch=forge.__main__:watch',
        ]
    }
)


setup(**config)

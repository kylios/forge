from forge import exc as forge_exec


def main():
    forge_exec.main()


def create_post():
    forge_exec.create_post()


def serve():
    forge_exec.serve()


def watch():
    forge_exec.watch()
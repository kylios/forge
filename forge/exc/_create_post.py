import argparse
import os
import sys

from forge import util


DESCRIPTION = "Create a new empty post in a Forge project."


METADATA_CONTENTS = '''
author = ""
title = ""
date = ""
'''


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        '-f', '--force',
        action='store_true',
        default=False,
        help="If passed, then any existing post with this name will be deleted.")
    parser.add_argument(
        '-s', '--sections',
        type=str,
        default='',
        help="A comma-separated list of section names (exclude the `.md` extension).")
    parser.add_argument(
        'content_type',
        type=str,
        nargs=1,
        help="The content type of the post.")
    parser.add_argument(
        'filename',
        type=str,
        nargs=1,
        help="The name of the folder for the post contents.")
    parser.add_argument(
        'path',
        type=str,
        nargs=1,
        help="The path of the Forge project.")
    return parser.parse_args()


def create_post():
    args = parse_args()

    assert len(args.path) == 1
    path = args.path[0]

    assert len(args.content_type) == 1
    content_type = args.content_type[0]

    assert len(args.filename) == 1
    filename = args.filename[0]

    sections = args.sections.split(',')

    post_path = os.path.join(path, 'content', content_type, filename)

    if args.force:
        util.ensure_deleted(post_path)

    if os.path.exists(post_path):
        sys.stderr.write("The path '{}' already exists.\nPass `--force` to overwrite it (WARNING: deletes all existing contents).\n".format(post_path))
        sys.exit(1)

    os.makedirs(post_path)

    # Create the images directory
    images_path = os.path.join(post_path, 'images')
    os.makedirs(images_path)

    # Copy the metadata contents
    metadata_path = os.path.join(post_path, 'meta.toml')
    metadata_contents = METADATA_CONTENTS

    for section in sections:
        # Append the section metadata to the metadata contents
        metadata_contents += "\n[{section}]\n".format(
            section=section)

        # Create the section file
        section_path = os.path.join(post_path, section + '.md')
        with open(section_path, 'w+') as f:
            pass

    # Write the metadata file
    with open(metadata_path, 'w') as f:
        f.write(metadata_contents)
import argparse
import logging
import os
import threading
import signal
import http.server
import time
from watchdog.events import FileSystemEventHandler, DirModifiedEvent
from watchdog.observers.polling import PollingObserver

import forge


DESCRIPTION = "Generate and serve a static site from a collection of markdown files."  # NOQA


logging.basicConfig(level=logging.DEBUG)


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'path',
        type=str,
        nargs=1,
        help="The path of the Forge project.")

    return parser.parse_args()


class Server(threading.Thread):

    def __init__(self, httpd):
        super(Server, self).__init__()
        self._httpd = httpd

    def run(self):
        logger.info("serving")
        self._httpd.serve_forever()
        logger.debug("done serving")


class ReloadHandler(FileSystemEventHandler):
    def __init__(self, path):
        super(ReloadHandler, self).__init__()
        self._path = path
        self._generating = threading.Event()

    def on_any_event(self, event):

        if self._generating.is_set():
            return

        # Don't do anything if the generated directory changes
        if 'generated' in event.src_path or \
                isinstance(event, DirModifiedEvent):
            logger.debug("Ignoring file system event")
            return

        logger.debug(
            "Event received: %s\n\t%s\n\t%s\n\t%s\n\t%s",
            repr(event),
            event.__dict__,
            event.event_type,
            event.is_directory,
            event.src_path)

        # Triggers the server to stop serving and reload
        logger.debug("ReloadHandler: generating site")
        self._generating.set()
        generated_path = forge.generate_site(self._path)
        os.chdir(generated_path)
        self._generating.clear()


def watch():

    args = parse_args()

    assert len(args.path) == 1
    path = os.path.abspath(args.path[0])

    generated_path = forge.generate_site(path)

    cur_path = os.getcwd()
    os.chdir(generated_path)

    stopped = threading.Event()

    http_handler = http.server.SimpleHTTPRequestHandler
    httpd = http.server.HTTPServer(("", 8000), http_handler)

    server = Server(httpd)

    logger.debug("Spawning server thread")
    server.start()

    observer_handler = ReloadHandler(path)
    observer = PollingObserver()
    observer.schedule(observer_handler, path, recursive=True)

    def shutdown(sig, frame):

        logger.debug("shut down signal received")
        observer.stop()

        httpd.shutdown()
        stopped.set()

    observer.start()

    signal.signal(signal.SIGINT, shutdown)
    signal.signal(signal.SIGTERM, shutdown)
    signal.signal(signal.SIGQUIT, shutdown)
    signal.signal(signal.SIGHUP, shutdown)

    while not stopped.is_set():
        time.sleep(1)

    observer.join()

    # Wait for the server to shut down
    server.join()

    # Clean up its mess
    logger.debug("Closing server")
    httpd.server_close()

    logger.debug("Done")
    os.chdir(cur_path)

import argparse
import logging
import os
import threading
import signal
import http.server
import time

import forge


DESCRIPTION = "Generate and serve a static site from a collection of markdown files."  # NOQA


logging.basicConfig(level=logging.DEBUG)


logger = logging.getLogger(__name__)


class Server(threading.Thread):

    def __init__(self, httpd):
        super(Server, self).__init__()
        self._httpd = httpd

    def run(self):
        logger.info("serving")
        self._httpd.serve_forever()
        logger.debug("done serving")


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'path',
        type=str,
        nargs=1,
        help="The path of the Forge project.")

    return parser.parse_args()


def serve():

    args = parse_args()

    assert len(args.path) == 1
    path = args.path[0]

    generated_path = forge.generate_site(path)

    cur_path = os.getcwd()
    os.chdir(generated_path)

    handler = http.server.SimpleHTTPRequestHandler
    httpd = http.server.HTTPServer(("", 8000), handler)

    stopped = threading.Event()

    def shutdown(sig, frame):

        logger.debug("shut down signal received")
        httpd.shutdown()
        stopped.set()

    signal.signal(signal.SIGINT, shutdown)
    signal.signal(signal.SIGTERM, shutdown)
    signal.signal(signal.SIGQUIT, shutdown)
    signal.signal(signal.SIGHUP, shutdown)

    server = Server(httpd)

    logger.debug("spawning server thread")
    server.start()

    # Loop while we wait for user input (ctrl-c)
    while not stopped.is_set():
        time.sleep(1)

    # Wait for the server to shut down
    server.join()

    # Clean up its mess
    logger.debug("closing server")
    httpd.server_close()

    os.chdir(cur_path)

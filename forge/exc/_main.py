import forge
import argparse


DESCRIPTION = "Generate a static site from a collection of markdown files and templates."


def parse_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'path',
        type=str,
        nargs=1,
        help="The path of the Forge project.")
    return parser.parse_args()


def main():
    args = parse_args()

    assert len(args.path) == 1
    path = args.path[0]

    forge.generate_site(path)
import codecs
import toml
import jinja2
import logging
import markdown
import os
import shutil

from forge import util


logger = logging.getLogger(__name__)


def get_template(templates_path, content_type):
    template_path = os.path.join(templates_path, content_type)
    content_template_path = os.path.join(template_path, "content.jinja2")
    toc_template_path = os.path.join(template_path, "toc.jinja2")

    logger.debug("Retrieving template: template_path=%s, content_template_path=%s, toc_template_path=%s",
                 template_path, content_template_path, toc_template_path)

    with open(content_template_path, 'r') as f:
        content_template = jinja2.Template(f.read())

    with open(toc_template_path, 'r') as f:
        toc_template = jinja2.Template(f.read())

    return dict(
        content=content_template,
        toc=toc_template
    )


def get_section_contents(section_path):
    logger.debug("Getting section contents: section_path=%s", section_path)
    md = markdown.Markdown()
    with codecs.open(section_path, mode="r", encoding="utf-8") as f:
        content = f.read()
    return md.convert(content)


def get_sections(post_path):
    logger.debug("Getting sections: post_path=%s", post_path)
    for d in os.listdir(post_path):
        logger.debug("  d=%s", d)
        path = os.path.join(post_path, d)
        name, ext = os.path.splitext(d)
        if ext == '.md':
            logger.debug("  found %s at %s", name, path)
            yield name, path


def get_metadata(path):
    path = os.path.join(path, 'meta.toml')
    with open(path, 'r') as f:
        return toml.load(f)


def _validate_post(content_type_path):
    logger.debug("Validating post %s", content_type_path)
    if not os.path.isdir(content_type_path):
        logger.debug("..not a directory")
        return False
    items = os.listdir(content_type_path)
    if 'meta.toml' not in items:
        logger.debug("..no 'meta.toml'")
        return False
    return True


def get_posts(content_type_path):
    for d in os.listdir(content_type_path):
        path = os.path.join(content_type_path, d)
        if _validate_post(path):
            yield d, path


def _validate_content_type(content_type_path):
    return os.path.isdir(content_type_path)


def get_content_types(content_path):
    for d in os.listdir(content_path):
        path = os.path.join(content_path, d)
        if _validate_content_type(path):
            yield d, path


def write_post_contents(dest_post_path, post_contents):
    index_path = os.path.join(dest_post_path, 'index.html')

    with codecs.open(index_path, mode='w', encoding="utf-8") as f:
        f.write(post_contents)


def write_content_type_toc(dest_content_type_path, toc_contents):
    index_path = os.path.join(dest_content_type_path, 'index.html')

    with codecs.open(index_path, mode='w', encoding="utf-8") as f:
        f.write(toc_contents)


def move_static(dest_post_path, static_path):
    dest_static_path = os.path.join(dest_post_path, 'static')
    if os.path.isdir(static_path):
        shutil.copytree(static_path, dest_static_path)


def generate(content_path, templates_path, generated_path):
    logger.debug("Generating: content_path=%s, templates_path=%s, generated_path=%s",
                 content_path, templates_path, generated_path)

    content_type_items = list()
    content_types = get_content_types(content_path)

    for content_type, content_type_path in sorted(content_types):
        logger.debug("content type: content_type=%s, content_type_path=%s",
                     content_type, content_type_path)

        templates = get_template(templates_path, content_type)
        content_template = templates['content']
        toc_template = templates['toc']

        post_items = list()
        posts = get_posts(content_type_path)

        content_type_item = dict(
            path=content_type + '/',
            metadata=get_metadata(content_type_path)
        )

        for post, post_path in sorted(posts):
            logger.debug("  post: post=%s, post_path=%s", post, post_path)

            sections = {
                section: get_section_contents(section_path)
                for section, section_path in get_sections(post_path)
            }

            metadata = get_metadata(post_path)
            static_path = os.path.join(post_path, 'static')

            context = sections
            context['metadata'] = metadata
            context['content_type'] = content_type_item

            post_contents = content_template.render(context=context, **context)

            dest_post_path = os.path.join(generated_path, content_type, post)
            util.ensure_dir(dest_post_path)

            write_post_contents(dest_post_path, post_contents)
            move_static(dest_post_path, static_path)

            post_items.append(dict(
                metadata=metadata,
                path=post + '/'
            ))

        content_type_item['items'] = post_items
        toc_contents = toc_template.render(
            context=content_type_item, **content_type_item)

        dest_content_type_path = os.path.join(generated_path, content_type)
        util.ensure_dir(dest_content_type_path)
        write_content_type_toc(dest_content_type_path, toc_contents)

        metadata = get_metadata(content_type_path)
        content_type_items.append(content_type_item)

    with open(os.path.join(templates_path, 'toc.jinja2'), 'r') as f:
        toc_template = jinja2.Template(f.read())
    toc_contents = toc_template.render(items=content_type_items)
    toc_path = os.path.join(generated_path, 'index.html')
    with codecs.open(toc_path, mode="w", encoding="utf-8") as f:
        f.write(toc_contents)


def move_static_files(static_path, generated_path):
    shutil.copytree(static_path, os.path.join(generated_path, 'static'))


def generate_site(path):
    content_path = os.path.join(path, 'content')
    static_path = os.path.join(path, 'static')
    templates_path = os.path.join(path, 'templates')
    generated_path = os.path.join(path, 'generated')

    # Ensure the generated_path is empty
    util.ensure_deleted(generated_path)

    generate(content_path, templates_path, generated_path)
    move_static_files(static_path, generated_path)

    return generated_path

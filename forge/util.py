import os
import shutil


def ensure_dir(path):
    # TODO: error checking to make sure it's not a file
    if os.path.isdir(path):
        return
    os.makedirs(path)


def ensure_deleted(path):
    if os.path.isdir(path):
        shutil.rmtree(path)

Forge
=====

A simple static-site generator.

Forge creates static websites from collections of markdown files. Forge is great for personal websites, where you might have different types of posts, such as blogs, project pages, or writing pieces. These posts are converted to html and placed into a heirarchical directory structure.

# Definitions

* post - An individual piece of content. A post may consist of one or multiple markdown files, and a collection of metadata.
* section - An individual markdown file belonging to a post.
* template - An html or other type of file, where the rendered markdown and metadata of the post is injected as a variable.
* content type - A type of post, where the posts are all rendered out using the same template and served in the same heirarchical level.

# Project directory layout

```
content/
  [content_type 1]/
    [post 1]/
      images/
        image1.jpg
        image2.jpg
      meta.toml
      section1.md
      section2.md

    [post 2]/
      images/
        image1.jpg
        image2.jpg
      meta.toml
      section1.md
      section2.md

    [...]


  [content_type 2]/
    [post 1]/
      images/
        image1.jpg
        image2.jpg
      meta.toml
      section1.md
      section2.md

    [post 2]/
      images/
        image1.jpg
        image2.jpg
      meta.toml
      section1.md
      section2.md

    [...]
  [...]

templates/
  [content_type 1].html.jinja2
  [content_type 2].html.jinja2

static/
  [...]
```

## Creating posts

When creating your project, you can use the command `forge-create-post` to help you add new posts.

```
forge-create-post <content_type> <filename> <path>
```

where:
* content_type is the type of post
* filename is the name of the directory to contain the post
* path is the path to the project directory

There are a couple optional arguments you may pass as well:
* `-f`, `--force` - Overwrite an existing post. By default, forge will not create a new post if `filename` is already used.
* `-s`, `--sections` - A comma-delimited list of sections to create. If passed, empty markdown files will be created with these section names.

For example, the command:

```
forge-create-post -f -s intro,body blog 04_creating_a_static_site projects/my_personal_blog
```

will create the following files and directory structure:

    projects/my_personal_blog/
        content/
            blog/
                04_creating_a_static_site/
                    images/
                    intro.md
                    body.md
                    meta.toml

(note that the project directory, `projects/my_personal_blog/`, should already exist)

## Templates

You will define two templates for each content type: `content.jinja2` and `toc.jinja2`. `content.jinja2` defines the template to use for the body of the post, while `toc.jinja2` is the template for the table-of-contents for that content type.

**content.jinja2**

Variables are made available according to the sections in the post. For example, our `blog` content type has the sections `intro` and `body`. Once these markdown files are rendered into html, they are made available to `content.jinja2` as variables named `intro` and `body` respectively.

**toc.jinja2**

This template takes one variable, `items`. `items` is an array with the following structure:

```
items = [
    {
        path: <string>,
        metadata: <object>
    },
    ...
    ...
]
```

* `path` - this element can be used for hyperlinks to the post's content page.
* `metadata` - the metadata object. More information follows in the next section.

## Metadata

You may want to assign some metadata to each of your posts, such as the date, title, etc. Use the file `meta.toml` for this.

`meta.toml` follows the [toml](https://github.com/toml-lang/toml) specification, which is similar to the `ini` format. All of the posts of the same content type should have the same metadata fields, but your template is the only enforcer of this guideline. Metadata fields are used in the template by injecting them as jinja2 variables. They are available under the `medatada` object, like so:

**meta.toml**

```toml
author = "Kyle Racette"
title = "Creating a Static Site"
date = "2017-09-10"
```

**blog/content.jinja**

```html
<html>
<head>
    <title>{{ metadata.title }}</title>
</head>
<body>
    <h1>{{ metadata.title }}</h1>
    By {{ metadata.author }} <br />
    Published on {{ metadata.date }} <br />
    <div class="intro">
    {{ intro }}
    </div>
    <div class="content">
    {{ body }}
    </div>
</body>
</html>
```

Note also that metadata is available inside the markdown-generated html as well. You have the opportunity to define section-specific metadata for this template injection by declaring sections inside `meta.toml` that match the sections you have created for your post. In this example, our `blog` content type has two sections: `intro` and `body`, each with respective markdown files. You may declare sections in `meta.toml` like so:

```toml
author = "Kyle Racette"
title = "Creating a Static Site"
date = "2017-09-10"

[intro]

main_image = "images/intro_main.jpg"

[body]

main_image = "images/body_main.jpg"
```

The variables defined under the `intro` section are injected into the converted html from `intro.md`, while the variables defined under the `body` section are injected into the converted html from `body.md`.

Metadata for each post is also available to the content type's table of contents template.

## Images

Images for a post are placed in the post's `images/` directory. They may be referenced using the path relative to that post, for example: `images/author_image.jpg`.

# Generating the Site

Once the content is in place, you can generate the site.

To generate a static site, run the command:

```
forge-generate /path/to/project
```

This will greate a new directory called `generated` inside the project's path.

To run a server, use the command:

```
forge-serve /path/to/project
```

This will generate the site, overwriting any previously generated content, and then run a web server to serve the contents of the site. It listens on `localhost:8000`.

The default server does not watch for changes to the filesystem. To run a server that reloads every time a change is made to a content or template file, use the command:

```
forge-watch /path/to/project
```